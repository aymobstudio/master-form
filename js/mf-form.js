class MfForm {
  constructor({ wrapper, errorOut = "#errorOut", zerobounce = false }) {
    this.wrapper = document.querySelector(wrapper);
    this.errorOut = document.querySelector(errorOut);
    this.inputs = Array("name", "email", "phone", "password");
    this.geoIpFields = Array(
      "mfpostal",
      "mfcountry",
      "mfcity",
      "mfcitizen",
      "mfuserip",
      "initdate"
    );
  }

  init() {
    this.getIP();
    this.buildFormUi();
    this.serializeData();
    this.initdate();
  }

  // Build form
  buildFormUi() {
    //create form
    const mfForm = document.createElement("form");
    mfForm.classList.add("mf-form");
    this.wrapper.appendChild(mfForm);

    for (let i = 0; i < this.inputs.length; i++) {
      //create form inputs wrapper
      const inputWrapper = document.createElement("div");
      inputWrapper.classList.add("mf-form-input");
      mfForm.appendChild(inputWrapper);

      //create error message wrapper
      const inputError = document.createElement("div");
      inputError.classList.add("mf-form-error");
      inputError.setAttribute("data-validate", this.inputs[i]);
      inputWrapper.appendChild(inputError);

      // create input
      const input = document.createElement("input");
      input.classList.add("mf-form-field");
      input.setAttribute("name", "mf" + this.inputs[i]);
      input.setAttribute("autocomplete", "off");
      if (this.inputs[i] == "password" || this.inputs[i] == "email") {
        input.setAttribute("type", this.inputs[i]);
      } else {
        input.setAttribute("type", "text");
      }
      input.setAttribute("placeholder", "Enter your " + this.inputs[i]);
      inputWrapper.appendChild(input);
    }
    //create submit button
    const mfFormSubmit = document.createElement("button");
    mfFormSubmit.classList.add("mf-form-submit");
    mfFormSubmit.innerText = "submit";
    mfForm.appendChild(mfFormSubmit);

    const loader = document.createElement("div");
    loader.classList.add("mf-form-loader");
    mfFormSubmit.appendChild(loader);

    // Create hidden inputs
    this.createHiddenIputs(mfForm);
  }

  // Hiden inputs
  createHiddenIputs(form) {
    for (let i = 0; i < this.geoIpFields.length; i++) {
      // create input
      const geoInput = document.createElement("input");
      geoInput.setAttribute("type", "hidden");
      geoInput.setAttribute("id", this.geoIpFields[i]);
      geoInput.setAttribute("name", this.geoIpFields[i]);
      form.prepend(geoInput);
    }
  }

  // Current timestamp
  initdate() {
    let timeStampInMs =
      window.performance &&
      window.performance.now &&
      window.performance.timing &&
      window.performance.timing.navigationStart
        ? window.performance.now() + window.performance.timing.navigationStart
        : Date.now();

    document.getElementById("initdate").value = timeStampInMs;
  }

  // Geo IP
  updateCityText(geoipResponse) {
    let countryName = geoipResponse.country.names.en || "";
    let cityName = geoipResponse.city.names.en || "";
    let cityPostal = geoipResponse.postal.code || "";
    let cityIso = geoipResponse.country.iso_code || "";
    let userIp = geoipResponse.traits.ip_address || "";

    document.getElementById("mfcountry").value = countryName;
    document.getElementById("mfcity").value = cityName;
    document.getElementById("mfpostal").value = cityPostal;
    document.getElementById("mfcitizen").value = cityIso;
    document.getElementById("mfuserip").value = userIp;
  }

  onSuccess(geoipResponse) {
    this.updateCityText(geoipResponse);
  }

  onError(error) {
    this.errorOut.innerHTML = "an error!  Please try again..";
  }

  getIP() {
    this.onSuccess = this.onSuccess.bind(this);
    this.onError = this.onError.bind(this);
    if (typeof geoip2 !== "undefined") {
      geoip2.city(this.onSuccess, this.onError);
    } else {
      this.errorOut.innerHTML = "a browser that blocks GeoIP2 requests";
    }
  }

  //Serialize inputs
  serializeData() {
    let forms = document.querySelectorAll(".mf-form");
    forms.forEach((form) => {
      form.addEventListener("submit", (e) => {
        e.preventDefault();
        let data = new FormData(form);
        let obj = {};
        for (let [key, value] of data) {
          if (obj[key] !== undefined) {
            if (!Array.isArray(obj[key])) {
              obj[key] = [obj[key]];
            }
            obj[key].push(value);
          } else {
            obj[key] = value;
          }
        }
        this.sendForm(form, obj);
      });
    });
  }

  //Send form
  sendForm(currForm, formObj) {
    fetch("http://form-api/api/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formObj),
    })
      .then((response) => response.json())
      .then((data) => {
        this.waitResponse(data, currForm);
      });
  }

  // Response
  waitResponse(data, form) {
    if (!data || !form) return;

    let formBtn = form.querySelector(".mf-form-submit");
    let formLoader = formBtn.querySelector(".mf-form-loader");

    formBtn.disabled = true;
    formLoader.style.display = "block";

    setTimeout(() => {
      this.formResponse(data, form);
    }, 800);
  }

  formResponse(data, form) {
    if (!data || !form) return;

    let formBtn = form.querySelector(".mf-form-submit");
    let formLoader = formBtn.querySelector(".mf-form-loader");
    form.querySelectorAll(`[data-validate]`).forEach((field) => {
      field.innerText = "";
    });

    if (data.status == true) {
      form.reset();
      this.errorOut.innerHTML = data.message;
    } else {
      if (data.type == "form") {
        this.errorOut.innerHTML = data.message;
      } else {
        this.displayErrors(data.message);
      }
    }

    formBtn.disabled = false;
    formLoader.style.display = "none";
  }

  //Display errors
  displayErrors(errors) {
    for (let key of Object.keys(errors)) {
      document.querySelector(`[data-validate="${key}"]`).innerText =
        errors[key];
    }
  }
}
