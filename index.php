<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP Validate</title>

  <link rel="stylesheet" type="text/css" href="./style/main.css">
</head>

<body>

  <div class="wrapper" id="customForm">
    <div id="errorOut"></div>
    <!-- <form action="<?php echo $_SERVER['PHP_SELF']; ?>" class="mf-form">
      <?php if (isset($_GET['clickid']) && ($_GET['clickid'] != "")) { ?>
        <input type="hidden" value="<?php echo $_GET['clickid']; ?>" name="clickid" />
      <?php } ?>
      <input type="hidden" id="initdate" name="initdate" value="<?php echo $initdate; ?>">
      <input type="hidden" id="mf-country" name="mf-country" value="<?php echo $geo['country']; ?>">
      <input type="hidden" id="mf-city" name="mf-city" value="<?php echo $geo['city']; ?>">
      <input type="hidden" id="mf-postal" name="mf-postal" value="<?php echo $geo['postal']; ?>">
      <input type="hidden" id="mf-citizen" name="mf-citizenship" value="<?php echo $geo['citizenship']; ?>">
      <input type="hidden" id="mf-userip" name="mf-userip" value="<?php echo $geo['ip']; ?>">

      <div class="mf-input-wrapper">
        <div class="mf-error-wrapper">
          <div class="mf-error">
            <div class="mf-error-msg"><?php echo $errors['username'] ?? '' ?></div>
          </div>
        </div>
        <input type="text" name="username" value="<?php echo htmlspecialchars($_POST['username']) ?? '' ?>" placeholder="Enter your First and Last Name">
      </div>

      <div class="mf-input-wrapper">
        <div class="mf-error-wrapper">
          <div class="mf-error">
            <div class="mf-error-msg"><?php echo $errors['email'] ?? '' ?></div>
          </div>
        </div>
        <input type="text" name="email" value="<?php echo htmlspecialchars($_POST['email']) ?? '' ?>" placeholder="Enter your Email">
      </div>

      <div class="mf-input-wrapper">
        <div class="mf-error-wrapper">
          <div class="mf-error">
            <div class="mf-error-msg"><?php echo $errors['phone'] ?? '' ?></div>
          </div>
        </div>
        <input type="text" name="phone" value="<?php echo htmlspecialchars($_POST['phone']) ?? '' ?>" placeholder="Phone number">
      </div>

      <div class="mf-input-wrapper">
        <div class="mf-error-wrapper">
          <div class="mf-error">
            <div class="mf-error-msg"><?php echo $errors['password'] ?? '' ?></div>
          </div>
        </div>
        <input type="password" name="password" value="<?php echo htmlspecialchars($_POST['password']) ?? '' ?>" placeholder="Your password">
      </div>

      <input type="submit" name="submit" value="submit">
    </form> -->

  </div>


  <script src="//geoip-js.com/js/apis/geoip2/v2.1/geoip2.js" type="text/javascript"></script>
  <script src="js/mf-form.js"></script>
  <script>
    const select = new MfForm({
      wrapper: '#customForm',
      errorOut: "#errorOut",
      zerobounce: true,
    }).init()
  </script>

  <script>

  </script>
</body>

</html>